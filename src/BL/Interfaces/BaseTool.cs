﻿using System.Windows.Input;
using System.Windows.Media;
using BL.Models;

namespace BL.Interfaces
{
	abstract class BaseTool
	{
		// Координаты на прошлом шаге
		protected int? _x;
		protected int? _y;

		/// <summary>
		/// Работать ли в координатах слоя, или же рабочей области
		/// </summary>
		public abstract bool UseLocalCoords { get; }


		/// <summary>
		/// Вызывается при включении инструмента
		/// </summary>
		public abstract void Start(Layer layer);


		/// <summary>
		/// Действие, совершаемое инструментом
		/// </summary>
		protected abstract void DoItOverride(Layer layer, Color color, int x, int y);


		/// <summary>
		/// Завершение работы инструмента
		/// </summary>
		public virtual void Stop(Layer layer)
		{
			_x = null;
			_y = null;

			Mouse.OverrideCursor = Cursors.Arrow;
		}


		/// <summary>
		/// Логика работы всех инструментов
		/// </summary>
		public void OnFrame(Layer layer, Color color, int x, int y)
		{
			if (_x == null || _y == null)
			{
				_x = x;
				_y = y;
			}

			DoItOverride(layer, color, x, y);

			_x = x;
			_y = y;
		}

	}
}
