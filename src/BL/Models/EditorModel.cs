﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Windows.Input;
using System.Windows.Media;
using BL.Interfaces;
using BL.Tools;
using BL.Utils;
using Catel.Data;

namespace BL.Models
{
	/// <summary>
	/// Класс редактора
	/// </summary>
    public class EditorModel : ModelBase
    {
		// Здесь мапятся ВСЕ инструменты
		private readonly Dictionary<Func<MouseEventArgs, bool>, BaseTool> _tools;

		private BaseTool _currentTool;
	    private int _layersCounter;
	    
		private readonly string _backupFile;

		/// <summary>
		/// Слои
		/// </summary>
		public ObservableCollection<Layer> Layers
        {
            get { return GetValue<ObservableCollection<Layer>>(LayersProperty); }
            set { SetValue(LayersProperty, value); }
        }
        public static readonly PropertyData LayersProperty = RegisterProperty("Layers", typeof(ObservableCollection<Layer>));


		/// <summary>
		/// Текущий слой
		/// </summary>
        public Layer ActiveLayer
        {
            get { return GetValue<Layer>(ActiveLayerProperty); }
            set { SetValue(ActiveLayerProperty, value); }
        }
        public static readonly PropertyData ActiveLayerProperty = RegisterProperty("ActiveLayer", typeof(Layer));


		/// <summary>
		/// Базовый цвет, применяемый во всех поддерживающих рисование инструментах
		/// </summary>
		public Color MainColor
		{
			get { return GetValue<Color>(MainColorProperty); }
			set { SetValue(MainColorProperty, value); }
		}
		public static readonly PropertyData MainColorProperty = RegisterProperty("MainColor", typeof(Color));


		/// <summary>
		/// ctor
		/// </summary>
		public EditorModel()
		{
			var commonFolderPath = Environment.GetFolderPath(Environment.SpecialFolder.CommonApplicationData);
			var appFolderPath = commonFolderPath + @"\LayersTool";
			if (!Directory.Exists(appFolderPath))
				Directory.CreateDirectory(appFolderPath);

			_backupFile = appFolderPath + @"\backup.bin";
			
	        _tools = new Dictionary<Func<MouseEventArgs, bool>, BaseTool>();

			_tools[args => args.IsLeftPressed()] = new PenTool();
			_tools[args => args.IsRightPressed()] = new MoveTool();
        }


		/// <summary>
		/// Добавить слой
		/// </summary>
        public void AddNewLayer()
        {
	        var newLayer = new Layer
	        {
		        Name = string.Format("Слой {0}", ++_layersCounter)
	        };

			Layers.Add(newLayer);
			ActiveLayer = newLayer;
        }


		/// <summary>
		/// Скопирвоать слой
		/// </summary>
	    public void CopyLayerCommand()
	    {
			var newLayer = new Layer(ActiveLayer)
			{
				Name = string.Format("{0} (копия)", ActiveLayer.Name)
			};

			Layers.Insert(0, newLayer);
			ActiveLayer = newLayer;
	    }


		/// <summary>
		/// Удалить слой
		/// </summary>
	    public void DeleteLayerCommand()
	    {
		    var activeLayerNum = Layers.IndexOf(ActiveLayer);

		    Layers.Remove(ActiveLayer);

			ActiveLayer = Layers.Any() ? Layers[Math.Min(activeLayerNum, Layers.Count - 1)] : null;
	    }


		/// <summary>
		/// Сдвинуть вверх
		/// </summary>
	    public void UpLayerCommand()
	    {
			var activeLayerNum = Layers.IndexOf(ActiveLayer);
			Layers.Move(activeLayerNum, activeLayerNum + 1);
	    }


		/// <summary>
		/// Сдвинуть вниз
		/// </summary>
	    public void DownLayerCommand()
	    {
			var activeLayerNum = Layers.IndexOf(ActiveLayer);
			Layers.Move(activeLayerNum, activeLayerNum - 1);
			
	    }


		#region Здесь редактор получает все нужные события ввода

		public void MouseDown(MouseEventArgs args)
        {
	        _currentTool = _tools.FirstOrDefault(tool => tool.Key(args)).Value;
	        _currentTool.Start(ActiveLayer);
        }

		public void MouseUp(MouseEventArgs args)
		{
			if (_currentTool != null)
			{
				_currentTool.Stop(ActiveLayer);
				_currentTool = null;
			}
		}

		public void MouseMove(MouseEventArgs args)
        {
			if (_currentTool == null)
				return;

			var image = ActiveLayer.Image;
			var pos = args.GetPosition(_currentTool.UseLocalCoords ? image : null);

			var x = (int)(pos.X * Layer.BaseResolution / image.ActualWidth);
			var y = (int)(pos.Y * Layer.BaseResolution / image.ActualHeight);

            _currentTool.OnFrame(ActiveLayer, MainColor, x, y);
        }

	    public void MouseLeave(MouseEventArgs args)
	    {
			MouseUp(args);		    
	    }

		#endregion


		/// <summary>
		/// Сериализация
		/// </summary>
		public void SaveToFile()
	    {
			var formatter = new BinaryFormatter();
			using (Stream stream = new FileStream(_backupFile, FileMode.Create, FileAccess.Write, FileShare.None))
		    {
			    formatter.Serialize(stream, Layers.Select(layer => new LayerDto(layer)).ToArray());
		    }
	    }


		/// <summary>
		/// Десериализация
		/// </summary>
	    public void LoadFromFile()
	    {
			var formatter = new BinaryFormatter();

			if (!File.Exists(_backupFile))
		    {
				Layers = new ObservableCollection<Layer>();
				AddNewLayer();
				return;
		    }

			using (Stream stream = new FileStream(_backupFile, FileMode.Open, FileAccess.Read, FileShare.None))
			{
				var layersDto = (LayerDto[])formatter.Deserialize(stream);
				Layers = new ObservableCollection<Layer>(layersDto.Select(dto => new Layer(dto)));
			}

		    ActiveLayer = Layers.LastOrDefault();
	    }
    }
}
