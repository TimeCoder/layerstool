﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using Catel.Data;

namespace BL.Models
{
	/// <summary>
	/// Класс слоя
	/// </summary>
    public class Layer : ModelBase 
    {
		// Базовый размер растра для рисования
        public const int BaseResolution = 2048;


		/// <summary>
		/// Название слоя
		/// </summary>
		public string Name
		{
			get { return GetValue<string>(NameProperty); }
			set { SetValue(NameProperty, value); }
		}
		public static readonly PropertyData NameProperty = RegisterProperty("Name", typeof(string));


		/// <summary>
		/// Рамка у слоя
		/// </summary>
		public Border Border
		{
			get { return GetValue<Border>(BorderProperty); }
			set { SetValue(BorderProperty, value); }
		}
		public static readonly PropertyData BorderProperty = RegisterProperty("Border", typeof(Border));


		/// <summary>
		/// Цвет рамки слоя
		/// </summary>
		public Brush BorderBrush
		{
			get { return GetValue<Brush>(BorderBrushProperty); }
			set { SetValue(BorderBrushProperty, value); }
		}
		public static readonly PropertyData BorderBrushProperty = RegisterProperty("BorderBrush", typeof(Brush), new SolidColorBrush(Colors.Transparent));


		/// <summary>
		/// Толщина рамки слоя
		/// </summary>
		public Thickness BorderThickness
		{
			get { return GetValue<Thickness>(BorderThicknessProperty); }
			set { SetValue(BorderThicknessProperty, value); }
		}
		public static readonly PropertyData BorderThicknessProperty = RegisterProperty("BorderThickness", typeof(Thickness), new Thickness(2.0));


		/// <summary>
		/// Изображение слоя (контрол)
		/// </summary>
		public Image Image
		{
			get { return GetValue<Image>(ImageProperty); }
			set { SetValue(ImageProperty, value); }
		}
		public static readonly PropertyData ImageProperty = RegisterProperty("Image", typeof(Image));


		/// <summary>
		/// Растр слоя
		/// </summary>
        public WriteableBitmap WBitmap
        {
            get { return GetValue<WriteableBitmap>(WBitmapProperty); }
            set { SetValue(WBitmapProperty, value); }
        }
        public static readonly PropertyData WBitmapProperty = RegisterProperty("WBitmap", typeof(WriteableBitmap));


		/// <summary>
		/// Видимость слоя
		/// </summary>
		public bool IsVisible
		{
			get { return GetValue<bool>(IsVisibleProperty); }
			set { SetValue(IsVisibleProperty, value); }
		}
		public static readonly PropertyData IsVisibleProperty = RegisterProperty("IsVisible", typeof(bool), true);


		/// <summary>
		/// Сдвиг слоя по оси X
		/// </summary>
		public double OffsetX
		{
			get { return GetValue<double>(OffsetXProperty); }
			set { SetValue(OffsetXProperty, value); }
		}
		public static readonly PropertyData OffsetXProperty = RegisterProperty("OffsetX", typeof(double));


		/// <summary>
		/// Сдвиг слоя по оси Y
		/// </summary>
		public double OffsetY
		{
			get { return GetValue<double>(OffsetYProperty); }
			set { SetValue(OffsetYProperty, value); }
		}
		public static readonly PropertyData OffsetYProperty = RegisterProperty("OffsetY", typeof(double));


		/// <summary>
		/// ctor1
		/// </summary>
        public Layer()
        {
            WBitmap = BitmapFactory.New(BaseResolution, BaseResolution);
			Construct();
        }


		/// <summary>
		/// copy ctor
		/// </summary>
		public Layer(Layer other)
		{
			WBitmap = other.WBitmap.Clone();
			Construct();

			IsVisible = other.IsVisible;
			OffsetX = 0;
			OffsetY = 0;
		}


		/// <summary>
		/// Мапинг DTO в объект слоя. С автомапером код был бы больше и сложнее
		/// </summary>
		public Layer(LayerDto dto)
		{
			WBitmap = dto.WBitmap;
			Construct();

			Name = dto.Name;
			IsVisible = dto.IsVisible;
			OffsetX = dto.OffsetX;
			OffsetY = dto.OffsetY;

			Border.RenderTransform = new TranslateTransform(OffsetX, OffsetY);
		}


		/// <summary>
		/// Основная инициализация слоя
		/// </summary>
	    private void Construct()
	    {
			Image = new Image
			{
				Stretch = Stretch.Fill,
				Source = WBitmap,
			};

		    Image.SetBinding(UIElement.VisibilityProperty, new Binding(IsVisibleProperty.Name)
		    {
			    Mode = BindingMode.TwoWay,
			    Converter = new BooleanToVisibilityConverter(),
			    Source = this
		    });

		    Border = new Border {Child = Image, BorderBrush = new SolidColorBrush(Colors.LightGray)};

		    Border.SetBinding(Border.BorderBrushProperty, new Binding(BorderBrushProperty.Name)
		    {
			    Mode = BindingMode.OneWay,
			    Source = this
		    });

		    Border.SetBinding(Border.BorderThicknessProperty, new Binding(BorderThicknessProperty.Name)
		    {
			    Mode = BindingMode.OneWay,
			    Source = this
		    });
	    }
    }
}
