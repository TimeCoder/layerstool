﻿using System;
using System.Runtime.Serialization;
using System.Windows.Media.Imaging;

namespace BL.Models
{
	/// <summary>
	/// Класс для сериализации слоя
	/// </summary>
	[Serializable]
	public class LayerDto : ISerializable
	{
		public WriteableBitmap WBitmap { get; private set; }
		public string Name { get; private set; }
		public bool IsVisible  { get; private set; }
		public double OffsetX { get; private set; }
		public double OffsetY { get; private set; }

		public LayerDto(Layer layer)
		{
			WBitmap = layer.WBitmap.Clone();
			Name = layer.Name;
			IsVisible = layer.IsVisible;
			OffsetX = layer.OffsetX;
			OffsetY = layer.OffsetY;
		}

		protected LayerDto(SerializationInfo info, StreamingContext context)
		{
			var data = info.GetValue("WBitmap", typeof(byte[]));
			WBitmap = BitmapFactory.New(Layer.BaseResolution, Layer.BaseResolution);
			WBitmap.FromByteArray((byte[])data);
			Name = info.GetString("Name");
			IsVisible = info.GetBoolean("IsVisible");
			OffsetX = info.GetDouble("OffsetX");
			OffsetY = info.GetDouble("OffsetY");
		}
		
		public void GetObjectData(SerializationInfo info, StreamingContext context)
		{
			info.AddValue("WBitmap", WBitmap.ToByteArray());
			info.AddValue("Name", Name);
			info.AddValue("IsVisible", IsVisible);
			info.AddValue("OffsetX", OffsetX);
			info.AddValue("OffsetY", OffsetY);
		}
	}
}
