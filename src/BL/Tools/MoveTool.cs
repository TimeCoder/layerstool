﻿using System.Windows;
using System.Windows.Input;
using System.Windows.Media;
using BL.Interfaces;
using BL.Models;


namespace BL.Tools
{
	/// <summary>
	/// Инструмент перемещения слоя
	/// </summary>
	class MoveTool : BaseTool
	{
		public override bool UseLocalCoords
		{
			get { return false; }
		}

		public override void Start(Layer layer)
		{
			Mouse.OverrideCursor = Cursors.Hand;

			layer.BorderThickness = new Thickness(2.0);
			layer.BorderBrush = new SolidColorBrush(Colors.OrangeRed);
		}


		protected override void DoItOverride(Layer layer, Color color, int x, int y)
		{
			layer.OffsetX += ((double)(x - _x) * layer.Image.ActualWidth / Layer.BaseResolution);
			layer.OffsetY += ((double)(y - _y) * layer.Image.ActualHeight / Layer.BaseResolution);

			layer.Border.RenderTransform = new TranslateTransform(layer.OffsetX, layer.OffsetY);
		}


		public override void Stop(Layer layer)
		{
			base.Stop(layer);
			layer.BorderBrush = new SolidColorBrush(Colors.LightGray);
		}
	}
}
