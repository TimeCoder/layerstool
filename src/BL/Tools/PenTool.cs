﻿using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using BL.Interfaces;
using BL.Models;

namespace BL.Tools
{
	/// <summary>
	/// Инструмент рисования
	/// </summary>
	class PenTool : BaseTool
	{
		public override bool UseLocalCoords
		{
			get { return true; }
		}


		public override void Start(Layer layer)
		{
			Mouse.OverrideCursor = Cursors.Pen;
		}


		protected override void DoItOverride(Layer layer, Color color, int x, int y)
		{
			layer.WBitmap.Lock();
			layer.WBitmap.DrawLineAa(_x.Value, _y.Value, x, y, color);
			layer.WBitmap.Unlock();
		}	
	}
}
