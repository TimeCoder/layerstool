﻿using System.Windows.Input;

namespace BL.Utils
{
	/// <summary>
	/// Полезные расширения для работы с мышью
	/// </summary>
	public static class MouseExtensions
	{
		public static bool IsLeftPressed(this MouseEventArgs args)
		{
			return  args.LeftButton == MouseButtonState.Pressed;
		}

		public static bool IsRightPressed(this MouseEventArgs args)
		{
			return args.RightButton == MouseButtonState.Pressed;
		}
	}
}
