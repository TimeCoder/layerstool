﻿using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Input;
using System.Windows.Media;
using BL.Models;
using Catel.Data;
using Catel.MVVM.Services;

namespace LayersTool.ViewModels
{
	using Catel.MVVM;

	/// <summary>
	/// ViewModel главного окна
	/// </summary>
	public class MainWindowViewModel : ViewModelBase
	{
		private readonly IMessageService _messageService;

		public override string Title
		{
			get { return "LayersTool"; }
			
		}

		/// <summary>
		/// Это модель - т.е. редактор
		/// </summary>
		[Model]
        public EditorModel Editor
        {
            get { return GetValue<EditorModel>(EditorProperty); }
            set { SetValue(EditorProperty, value); }
        }
        public static readonly PropertyData EditorProperty = RegisterProperty("Editor", typeof(EditorModel));


        [ViewModelToModel("Editor")]
        public ObservableCollection<Layer> Layers
        {
            get { return GetValue<ObservableCollection<Layer>>(LayersProperty); }
            set { SetValue(LayersProperty, value); }
        }
        public static readonly PropertyData LayersProperty = RegisterProperty("Layers", typeof(ObservableCollection<Layer>));


		[ViewModelToModel("Editor")]
		public Color MainColor
		{
			get { return GetValue<Color>(MainColorProperty); }
			set { SetValue(MainColorProperty, value); }
		}
		public static readonly PropertyData MainColorProperty = RegisterProperty("MainColor", typeof(Color));


		/// <summary>
		/// ctor
		/// </summary>
		public MainWindowViewModel(IMessageService messageService)
		{
			_messageService = messageService;
			Editor = new EditorModel();
			Editor.LoadFromFile();
			MainColor = Colors.DarkOrchid;
		}

		#region Команды мыши
		
		private Command<MouseEventArgs> _mouseDownCommand;
		public Command<MouseEventArgs> MouseDownCommand
        {
            get
            {
				return _mouseDownCommand ?? (_mouseDownCommand = 
					new Command<MouseEventArgs>(args => Editor.MouseDown(args)));
            }
        }

        private Command<MouseEventArgs> _mouseUpCommand;
		public Command<MouseEventArgs> MouseUpCommand
        {
            get
            {
				return _mouseUpCommand ?? (_mouseUpCommand = 
					new Command<MouseEventArgs>(args => Editor.MouseUp(args)));
            }
        }


		private Command<MouseEventArgs> _mouseMoveCommand;
		public Command<MouseEventArgs> MouseMoveCommand
		{
			get
			{
				return _mouseMoveCommand ?? (_mouseMoveCommand = 
					new Command<MouseEventArgs>(args => Editor.MouseMove(args)));
			}
		}

		private Command<MouseEventArgs> _mouseLeaveCommand;
		public Command<MouseEventArgs> MouseLeaveCommand
		{
			get
			{
				return _mouseLeaveCommand ?? (_mouseLeaveCommand =
					new Command<MouseEventArgs>(args => Editor.MouseLeave(args)));
			}
		}

		#endregion


		#region Команды действий

		private Command _newLayerCommand;
		public Command NewLayerCommand
		{
			get
			{
				return _newLayerCommand ?? (_newLayerCommand = new Command(
					() => Editor.AddNewLayer()));
			}
		}
		
		private Command _copyLayerCommand;
		public Command CopyLayerCommand
		{
			get
			{
				return _copyLayerCommand ?? (_copyLayerCommand = new Command(
					() => Editor.CopyLayerCommand(),
					() => Editor.ActiveLayer != null));
			}
		}
		
		private Command _deleteLayerCommand;
		public Command DeleteLayerCommand
		{
			get
			{
				return _deleteLayerCommand ?? (_deleteLayerCommand = new Command(
					() => Editor.DeleteLayerCommand(),
					() => Editor.ActiveLayer != null));
			}
		}
		
		private Command _upLayerCommand;
		public Command UpLayerCommand
		{
			get
			{
				
				return _upLayerCommand ?? (_upLayerCommand = new Command(
					() => Editor.UpLayerCommand(),
					() => Editor.ActiveLayer != null && Layers.Last() != Editor.ActiveLayer));
			}
		}

		private Command _downLayerCommand;
		public Command DownLayerCommand
		{
			get
			{
				return _downLayerCommand ?? (_downLayerCommand = new Command(
					() => Editor.DownLayerCommand(),
					() => Editor.ActiveLayer != null && Layers.First() != Editor.ActiveLayer));
			}
		}

		private Command _helpCommand;
		public Command HelpCommand
		{
			get
			{
				return _helpCommand ?? (_helpCommand = new Command(
					() => _messageService.ShowInformation("ЛКМ - карандаш, ПКМ - перемещение слоев")));
			}
		}

		#endregion

		/// <summary>
		/// Сохраняем документ на выходе
		/// </summary>
		protected override bool Cancel()
		{
			Editor.SaveToFile();
			return base.Cancel();
		}
	}
}
