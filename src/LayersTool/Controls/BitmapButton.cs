﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace LayersTool.Controls
{
	/// <summary>
	/// Класс кнопки с картинкой
	/// </summary>
	public class BitmapButton : Button
	{
		static BitmapButton()
		{
			DefaultStyleKeyProperty.OverrideMetadata(typeof(BitmapButton),
				new FrameworkPropertyMetadata(typeof(BitmapButton)));
		}


		public ImageSource Icon
		{
			get { return (ImageSource)GetValue(IconProperty); }
			set { SetValue(IconProperty, value); }
		}


		public static readonly DependencyProperty IconProperty =
			DependencyProperty.Register("Icon", typeof(ImageSource), typeof(BitmapButton), new PropertyMetadata(null));
	}
}
